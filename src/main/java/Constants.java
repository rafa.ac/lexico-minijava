public class Constants {

    public static final String ErroAbreChaves = "falta abrir chaves";
    public static final String ErroFechamentoChaves = "falta fechar chaves";

    public static final String ErroFaltaAberturaColchetes = "faltando abrir colchetes";
    public static final String ErroFaltaFechamentoColchetes = "faltando fechar colchetes";

    public static final String ErroFaltaAbrirParenteses = "faltando abrir parenteses";
    public static final String ErroFaltaFecharParenteses = "faltando fechar parenteses";


    public static final String ErroDeclaracaoClasse = "falta declarar a classe";
    public static final String ErroNomeClasse = "falta o nome da classe";
    public static final String ErroAcessoPublicMain = "método main precisa ser public";
    public static final String ErroMainStatic = "método main precisa ser static";
    public static final String ErroMainVoid = "método main precisa ter retorno do tipo void";
    public static final String ErroMetodoMain = "faltando método main";
    public static final String ErroParametroString = "faltando parametro do tipo String";
    public static final String ErroFaltaNomeDoParametro = "faltando nome do parametro";
    public static final String ErroBlocoInvalido = "bloco de código inválido";
    public static final String ErroFaltaNomeVariavel = "faltando o nome da variável";
    public static final String ErroFaltaTipoVariavel = "faltando o tipo da variável";

    public static final String ErroFaltaAtribuicao = "faltando atribuição da variável";
    public static final String ErroFaltaValor = "faltando valor da variável";
    public static final String ErroFaltaPontoVirgula = "faltando ponto e vírgula";
    public static final String ErroFaltaOperador = "faltando operador";
    public static final String ErroChamadaInvalida = "chamada inválida";
    public static final String ErroAbreParenteses = "faltando abrir parenteses";
}
