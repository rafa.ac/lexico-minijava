public enum Tipo {
    TK_CLASS,                   // class
    TK_IDENTIFICADOR,           // nome da classe
    TK_ACESSO_PUBLIC,           // public
    TK_ACESSO_PRIVATE,          // private
    TK_STATIC,
    TK_NUMERO_INTEIRO,
    TK_VOID,
    TK_MAIN,
    TK_ABRE_CHAVES,             // {
    TK_FECHA_CHAVES,            // }
    TK_ABRE_PARENTESES,
    TK_FECHA_PARENTESES,
    TK_ABRE_COLCHETES,
    TK_FECHA_COLCHETES,
    TK_PONTO,
    TK_PONTO_VIRGULA,
    TK_VIRGULA,
    TK_EXCLAMACAO,
    TK_TIPO_STRING,
    TK_TIPO_INT,
    TK_TIPO_BOOL,
    TK_TIPO_BOOL_TRUE,
    TK_TIPO_BOOL_FALSE,
    TK_FUNCAO_WRITE,
    TK_IF,
    TK_ELSE,
    TK_RETURN,
    TK_NEW,
    TK_OP_MENOR,
    TK_OP_MAIOR,
    TK_OP_IGUAL,
    TK_OP_ADICAO,
    TK_OP_SUBTRACAO,
    TK_OP_MULTIPLICACAO,
    TK_OP_DIVISAO,
    TK_OP_MAIOR_IGUAL,
    TK_OP_MENOR_IGUAL,
    TK_OP_MOD,
    TK_OP_DIFERENTE,
    TK_OP_ASSIGN,
    TK_OP_AND,
    TK_OP_OR,
    TK_OP_NEGACAO
}
