import java.io.*;
import java.util.ArrayList;

class Lexico {
    ArrayList<Token> tokenList = new ArrayList<>();

    ArrayList<Token> analisa(String filepath) {
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        PushbackReader pushbackReader = null;

        try {
            fileInputStream = new FileInputStream(filepath);
            inputStreamReader = new InputStreamReader(fileInputStream, "US-ASCII");
            pushbackReader = new PushbackReader(inputStreamReader);
            Character ch = null;
            int linha = 1;

            do {
                String lexema = "";
                ch = getChar(pushbackReader);
                Token token = null;

                if (ch == null) {
                    System.out.println("fim do arquivo");
                    break;
                } else if (!String.valueOf(ch).matches(".")) {
                    linha++;
                    getChar(pushbackReader);
                    continue;
                } else if (Character.isWhitespace(ch)) {
                    continue;
                }

                if (Character.isLetter(ch)) {
                    lexema += ch;
                    ch = getChar(pushbackReader);

                    while (ch != null && (Character.isLetterOrDigit(ch) || ch.equals('_'))) {
                        lexema += ch;
                        ch = getChar(pushbackReader);
                    }

                    if (ch != null)
                        pushbackReader.unread(ch);

                    token = new Token(lexema);
                    token.setTipo(getTipo(token.getLexema()));
                    token.setLinha(linha);
                    tokenList.add(token);

                    continue;
                } else if (Character.isDigit(ch)) {
                    lexema += ch;
                    ch = getChar(pushbackReader);

                    while (Character.isDigit(ch)) {
                        lexema += ch;
                        ch = getChar(pushbackReader);
                    }

                    pushbackReader.unread(ch);

                    token = new Token(Tipo.TK_NUMERO_INTEIRO, lexema);
                    token.setLinha(linha);
                    tokenList.add(token);

                    continue;
                } else if (ch.equals('/')) {
                    ch = getChar(pushbackReader);
                    if (ch.equals('/')) {
                        ch = getChar(pushbackReader);

                        while (String.valueOf(ch).matches(".")) {
                            ch = getChar(pushbackReader);
                        }
                        continue;
                    }
                }

                switch (ch) {
                    case '(':
                        token = new Token(Tipo.TK_ABRE_PARENTESES, ch.toString());
                        break;
                    case ')':
                        token = new Token(Tipo.TK_FECHA_PARENTESES, ch.toString());
                        break;
                    case '{':
                        token = new Token(Tipo.TK_ABRE_CHAVES, ch.toString());
                        break;
                    case '}':
                        token = new Token(Tipo.TK_FECHA_CHAVES, ch.toString());
                        break;
                    case '[':
                        token = new Token(Tipo.TK_ABRE_COLCHETES, ch.toString());
                        break;
                    case ']':
                        token = new Token(Tipo.TK_FECHA_COLCHETES, ch.toString());
                        break;
                    case ';':
                        token = new Token(Tipo.TK_PONTO_VIRGULA, ch.toString());
                        break;
                    case '.':
                        token = new Token(Tipo.TK_PONTO, ch.toString());
                        break;
                    case '!':
                        token = new Token(Tipo.TK_EXCLAMACAO, ch.toString());
                        break;
                    case ',':
                        token = new Token(Tipo.TK_VIRGULA, ch.toString());
                        break;
                    case '=':
                        token = new Token(Tipo.TK_OP_ASSIGN, ch.toString());
                        break;
                    case '*':
                        token = new Token(Tipo.TK_OP_MULTIPLICACAO, ch.toString());
                        break;
                    case '-':
                        token = new Token(Tipo.TK_OP_SUBTRACAO, ch.toString());
                        break;
                    case '+':
                        token = new Token(Tipo.TK_OP_ADICAO, ch.toString());
                        break;
                    case '/':
                        token = new Token(Tipo.TK_OP_DIVISAO, ch.toString());
                        break;
                    case '<':
                        token = new Token(Tipo.TK_OP_MENOR, ch.toString());
                        break;
                    case '>':
                        token = new Token(Tipo.TK_OP_MAIOR, ch.toString());
                        break;
                    default:
                        throw new RuntimeException("Caracter inválido: " + ch);
                }

                if (token != null) {
                    token.setLinha(linha);
                    tokenList.add(token);
                }


            } while (ch != null);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
                inputStreamReader.close();
                pushbackReader.close();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (Token t : tokenList) {
            System.out.println(t.toString());
        }

        return tokenList;
    }

    public Token getToken() {
        Token token = null;

        if (tokenList.size() > 0) {
            token = tokenList.get(0);
            tokenList.remove(0);
        }

        return token;
    }

    private Character getChar(PushbackReader pushbackReader) throws IOException {
        Character c = null;
        int i = pushbackReader.read();

        if (i != -1) {
            c = (char) i;
        }
        return c;
    }

    private Tipo getTipo(String lexema) {
        switch (lexema) {
            case "class":
                return Tipo.TK_CLASS;
            case "public":
                return Tipo.TK_ACESSO_PUBLIC;
            case "return":
                return Tipo.TK_RETURN;
            case "static":
                return Tipo.TK_STATIC;
            case "void":
                return Tipo.TK_VOID;
            case "String":
                return Tipo.TK_TIPO_STRING;
            case "int":
                return Tipo.TK_TIPO_INT;
            case "main":
                return Tipo.TK_MAIN;
            case "new":
                return Tipo.TK_NEW;
            case "System":
                return Tipo.TK_FUNCAO_WRITE;
            case "if":
                return Tipo.TK_IF;
            case "else":
                return Tipo.TK_ELSE;
            case "(":
                return Tipo.TK_ABRE_PARENTESES;
            case ")":
                return Tipo.TK_FECHA_PARENTESES;
            case "{":
                return Tipo.TK_ABRE_CHAVES;
            case "}":
                return Tipo.TK_FECHA_CHAVES;
            case "[":
                return Tipo.TK_ABRE_COLCHETES;
            case "]":
                return Tipo.TK_FECHA_COLCHETES;
            default:
                return Tipo.TK_IDENTIFICADOR;
        }
    }
}
