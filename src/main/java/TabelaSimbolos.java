import java.util.HashMap;

public class TabelaSimbolos {
    private HashMap<String, Token> tabela;

    public TabelaSimbolos() {
        this.tabela = new HashMap<>();
    }

    public HashMap<String, Token> getTabela() {
        return tabela;
    }

    public void setTabela(HashMap<String, Token> tabela) {
        this.tabela = tabela;
    }
}
