import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Token> tokens = new ArrayList<Token>();
        Lexico lex = new Lexico();

        Scanner input = new Scanner(System.in);
        System.out.println("insira o caminho absoluto do arquivo");
        String path = input.next();

        lex.analisa(path);

        Sintatico analisadorSintatico = new Sintatico(lex);
        boolean resp = analisadorSintatico.analisaClasse();

        if (resp) {
            System.out.println("Classe válida!");
            System.out.println("Gerando bytecode");

            generateByteCode(path);
        }

//        for (Token t : tokens) {
//            System.out.println(t.toString());
//        }
    }

    public static void generateByteCode(String pathToFile) {
        try{
            Runtime run = Runtime.getRuntime();
            String gerarByteCode = "javac " + pathToFile;
            run.exec(gerarByteCode);
            System.out.println("bytecode gerado com sucesso!");
            System.out.println("arquivo " + pathToFile.replace(".java", ".class"));
        }catch(Exception err){
            System.out.println("Erro na geração de byecode " + err.getMessage());
        }
    }
}
