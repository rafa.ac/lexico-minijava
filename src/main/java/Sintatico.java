public class Sintatico {
    private TabelaSimbolos tabelaSimbolos;
    private Lexico analisadorLexico;
    private Semantico analisadorSemantico;
    private Token token;

    public Sintatico(Lexico analisadorLexico) {
        this.tabelaSimbolos = new TabelaSimbolos();
        this.analisadorLexico = analisadorLexico;
        this.analisadorSemantico = new Semantico();
    }

    private void buscaToken() {
        token = analisadorLexico.getToken();
    }

    private void imprimeErro(String msg) {
        System.out.println(msg + " na linha " + token.getLinha());
    }

    public boolean analisaClasse() {
        boolean resp = false;
        buscaToken();

        if (token.getTipo().equals(Tipo.TK_CLASS)) {
            buscaToken();

            if (token.getTipo().equals(Tipo.TK_IDENTIFICADOR)) {
                insereIdentificador("nome-classe");
                buscaToken();

                if (token.getTipo().equals(Tipo.TK_ABRE_CHAVES)) {
                    buscaToken();

                    if (analisaBlocoMain()) {
                        if (token.getTipo().equals(Tipo.TK_FECHA_CHAVES)) {
                            resp = true;
                        } else {
                            imprimeErro(Constants.ErroFechamentoChaves);
                        }
                    }
                } else {
                    imprimeErro(Constants.ErroAbreChaves);
                }
            } else {
                imprimeErro(Constants.ErroNomeClasse);
            }
        } else {
            imprimeErro(Constants.ErroDeclaracaoClasse);
        }

        return resp;
    }

    private boolean analisaBlocoMain() {
        boolean resp = false;

        if (token.getTipo().equals(Tipo.TK_ACESSO_PUBLIC)) {
            buscaToken();

            if (token.getTipo().equals(Tipo.TK_STATIC)) {
                buscaToken();

                if (token.getTipo().equals(Tipo.TK_VOID)) {
                    buscaToken();

                    if (token.getTipo().equals(Tipo.TK_MAIN)) {
                        buscaToken();

                        if (token.getTipo().equals(Tipo.TK_ABRE_PARENTESES)) {
                            buscaToken();

                            if (token.getTipo().equals(Tipo.TK_TIPO_STRING)) {
                                buscaToken();

                                if (token.getTipo().equals((Tipo.TK_ABRE_COLCHETES))) {
                                    buscaToken();

                                    if (token.getTipo().equals(Tipo.TK_FECHA_COLCHETES)) {
                                        buscaToken();

                                        if (token.getTipo().equals(Tipo.TK_IDENTIFICADOR)) {
                                            insereIdentificador("args");
                                            buscaToken();

                                            if (token.getTipo().equals(Tipo.TK_FECHA_PARENTESES)) {
                                                buscaToken();

                                                if (token.getTipo().equals(Tipo.TK_ABRE_CHAVES)) {
                                                    buscaToken();

                                                    if (analisaBlocoCodigo()) {
                                                        if (token.getTipo().equals(Tipo.TK_FECHA_CHAVES)) {
                                                            resp = true;
                                                        } else {
                                                            imprimeErro(Constants.ErroFechamentoChaves);
                                                        }
                                                    }
                                                } else {
                                                    imprimeErro(Constants.ErroAbreChaves);
                                                }
                                            } else {
                                                imprimeErro(Constants.ErroFaltaFecharParenteses);
                                            }
                                        } else {
                                            imprimeErro(Constants.ErroFaltaNomeDoParametro);
                                        }
                                    } else {
                                        imprimeErro(Constants.ErroFaltaFechamentoColchetes);
                                    }
                                } else {
                                    imprimeErro(Constants.ErroFaltaAberturaColchetes);
                                }
                            } else {
                                imprimeErro(Constants.ErroParametroString);
                            }
                        } else {
                            imprimeErro(Constants.ErroFaltaAbrirParenteses);
                        }
                    } else {
                        imprimeErro(Constants.ErroMetodoMain);
                    }
                } else {
                    imprimeErro(Constants.ErroMainVoid);
                }
            } else {
                imprimeErro(Constants.ErroMainStatic);
            }
        } else {
            imprimeErro(Constants.ErroAcessoPublicMain);
        }

        return resp;
    }

    private boolean analisaBlocoCodigo() {
        boolean resp = false;

        if (token.getTipo().equals(Tipo.TK_TIPO_INT) || token.getTipo().equals(Tipo.TK_TIPO_BOOL)) {
            resp = analisaVariavel();
        } else if (token.getTipo().equals(Tipo.TK_FUNCAO_WRITE)) {
          resp = analisaWrite();
        } else if (token.getTipo().equals(Tipo.TK_IDENTIFICADOR) || token.getTipo().equals(Tipo.TK_NUMERO_INTEIRO)) {
            resp = analisaIdentificadorNumero(); // adicionar analisa chamada método?
        } else if (token.getTipo().equals(Tipo.TK_FECHA_CHAVES)) {
            buscaToken();

            if (token.getTipo().equals(Tipo.TK_FECHA_CHAVES)) {
                resp = true;
            }
        }

        if (!resp) {
            imprimeErro(Constants.ErroBlocoInvalido);
        }

        return resp;
    }

    private Boolean analisaWrite() {
        boolean resp = false;

        if (token.getTipo().equals(Tipo.TK_FUNCAO_WRITE)) {
            buscaToken();
            buscaToken();
            buscaToken();
            buscaToken();
            buscaToken();

            if (token.getTipo().equals(Tipo.TK_ABRE_PARENTESES)) {
                buscaToken();

                if (analisaBlocoCodigo()) {
                    if (token.getTipo().equals(Tipo.TK_FECHA_PARENTESES)) {
                        buscaToken();

                        if (token.getTipo().equals(Tipo.TK_PONTO_VIRGULA)) {
                            buscaToken();

                            return analisaBlocoCodigo();
                        } else {
                            imprimeErro(Constants.ErroFaltaPontoVirgula);
                        }
                    } else {
                        imprimeErro(Constants.ErroFaltaFecharParenteses);
                    }
                }
            } else {
                imprimeErro(Constants.ErroAbreParenteses);
            }
        } else {
            imprimeErro(Constants.ErroChamadaInvalida);
        }

        return resp;
    }

    private boolean analisaVariavel() {
        boolean resp = false;

        if (token.getTipo().equals(Tipo.TK_TIPO_INT) || token.getTipo().equals(Tipo.TK_TIPO_BOOL)) {
            String lexemaVariavel = token.getLexema();
            buscaToken();

            if (token.getTipo().equals(Tipo.TK_IDENTIFICADOR)) {
                insereIdentificador(lexemaVariavel);
                buscaToken();

                if (token.getTipo().equals(Tipo.TK_OP_ASSIGN)) {
                    buscaToken();

                    if (token.getTipo().equals(Tipo.TK_NUMERO_INTEIRO)
                            || token.getTipo().equals(Tipo.TK_TIPO_BOOL_TRUE)
                            || token.getTipo().equals(Tipo.TK_TIPO_BOOL_FALSE)) {
                        buscaToken();

                        if (token.getTipo().equals(Tipo.TK_PONTO_VIRGULA)) {
                            buscaToken();

                            resp = analisaBlocoCodigo();
                        } else {
                            imprimeErro(Constants.ErroFaltaPontoVirgula);
                        }
                    } else {
                        imprimeErro(Constants.ErroFaltaValor);
                    }
                } else {
                    imprimeErro(Constants.ErroFaltaAtribuicao);
                }
            } else {
                imprimeErro(Constants.ErroFaltaNomeVariavel);
            }
        } else {
            imprimeErro(Constants.ErroFaltaTipoVariavel);
        }

        return resp;
    }

    private boolean analisaIdentificadorNumero() {
        boolean resp = false;

        if (token.getTipo().equals(Tipo.TK_IDENTIFICADOR) || token.getTipo().equals(Tipo.TK_NUMERO_INTEIRO)) {
            String op1 = token.getLexema();
            buscaToken();

            if (token.getTipo().equals(Tipo.TK_OP_ADICAO)
                    || token.getTipo().equals(Tipo.TK_OP_SUBTRACAO)
                    || token.getTipo().equals(Tipo.TK_OP_MULTIPLICACAO)
                    || token.getTipo().equals(Tipo.TK_OP_DIVISAO)) {

                resp = analisaOperacao(op1);
            } else {
                imprimeErro(Constants.ErroFaltaOperador);
            }

            resp = true;
        } else {
            imprimeErro(Constants.ErroFaltaOperador);
        }

        return resp;
    }

    private boolean analisaOperacao(String op1) {
        boolean resp = false;

        Token operacao = token;
        buscaToken();

        if (token.getTipo().equals(Tipo.TK_IDENTIFICADOR) || token.getTipo().equals(Tipo.TK_NUMERO_INTEIRO)) {
            String op2 = token.getLexema();
            buscaToken();

            analisadorSemantico.valida(op1, op2, operacao.getLexema(), tabelaSimbolos);

            if (token.getTipo().equals(Tipo.TK_PONTO_VIRGULA)) {
                buscaToken();
                resp = analisaBlocoCodigo();
            } else {
                imprimeErro(Constants.ErroFaltaPontoVirgula);
            }
        } else {
            imprimeErro(Constants.ErroFaltaOperador);
        }

        return resp;
    }

    private void insereIdentificador(String identificador) {
        token.setIdentificador(identificador);
        tabelaSimbolos.getTabela().put(token.getLexema(), token);
    }
}
