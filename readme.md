Projeto de compiladores desenvolvido em Java 8. O compilador irá ler e validar um arquivo contendo um programa em 
minijava e compilar para bytecode usando javac.


Principais classes:
- Main 
    - Inicia as análises e gera o arquivo de bytecode
- Lexico 
    - Essa classe contém o analisador léxico em si, com as verificações necessárias.
- Sintatico
    - Classe com as validações e análise sintática.
- Semantico
    - Classe com as validações e análise semântica
- Constants
    - Classe estática contendo as strings utilizadas no projeto
- TabelaSimbolos
    - Classe representando a tabela de símbolos
- Tipo
    - Arquivo contendo o Enum de todos os tipos necessários.
- Token
    - Model contendo as informações do token.




Ao executar o programa, o console irá exibir uma mensagem pedindo para informar o arquivo a ser lido.
Deve ser passado o caminho absoluto do arquivo. O arquivo .class será gerado no mesmo diretório. 

Ex:
 
Caminho informado: C:\git\lexico-minijava\src\main\java\Add.java 
Arquivo gerado: C:\git\lexico-minijava\src\main\java\Add.class